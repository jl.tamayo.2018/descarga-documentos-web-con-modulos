#!/usr/bin/python3

import urllib.request

class Robot:

    def __init__(self, url):
        self.url = url
        self.downloaded = False

    def retrieve(self):
        if not self.downloaded:
            print("Descargando url")
            endpoint = urllib.request.urlopen(self.url)
            self.contenido = endpoint.read().decode('utf-8')
            self.downloaded = True

    def content(self):
        self.retrieve()
        return self.contenido

    def show(self):
        print(self.content())

if __name__ == "__main__":
    robot = Robot("https://www.urjc.es/")
    print(robot.url)
    robot.show()
    robot.retrieve()