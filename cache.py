#!/usr/bin/python3

from robot import Robot

class Cache:
    def __init__(self):
        self.cache = {}
    def retrieve(self, url):
        if url not in self.cache:
            r = Robot(url)
            self.cache[url] = r
    def show(self, url):
        print(self.content(url))

    def show_all(self):
        for url in self.cache:
            print(url)

    def content(self, url):
        self.retrieve(url)
        return self.cache[url].content()

if __name__ == "__main__":
    cache = Cache()
    cache.retrieve('https://www.urjc.es/')
    cache.show('https://www.aulavirtual.urjc.es')
    cache.show_all()
